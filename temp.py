import numbers
from flask import Flask, request
import base64, email.utils, hmac, hashlib, urllib
import requests

app = Flask(__name__)


def sign(method, host, path, params, skey, ikey):
    """
    Return HTTP Basic Authentication ("Authorization" and "Date") headers.
    method, host, path: strings from request
    params: dict of request parameters
    skey: secret key
    ikey: integration key
    """
    # create canonical string
    now = email.utils.formatdate()
    canon = [now, method.upper(), host.lower(), path]
    args = []
    for key in sorted(params.keys()):
        val = params[key].encode("utf-8")
        args.append(
            '%s=%s' % (urllib.parse.
                       quote(key, '~'), urllib.parse.quote(val, '~')))
    canon.append('&'.join(args))
    canon = '\n'.join(canon)
    # sign canonical string
    sig = hmac.new(bytes(skey, encoding='utf-8'),
                   bytes(canon, encoding='utf-8'),
                   hashlib.sha1)
    auth = '%s:%s' % (ikey, sig.hexdigest())
    # return headers
    return {'Date': now, 'Authorization': 'Basic %s' % base64.b64encode(bytes(auth, encoding="utf-8")).decode()}
header1 = sign("GET","api-22a9c2b4.duosecurity.com","/admin/v1/users",{},"9m2ySNGIjxlH19m5774ISucb6Bs7xjBW2579neNh","DIS7GXLUI8WCHZU8R0GV")
res = requests.get("https://api-22a9c2b4.duosecurity.com/admin/v1/users",headers=header1)

data = res.json()

names = []
numbers = []
phone_ids = []
resp = {}
phone ={}
for i in data["response"]:
    for j in i["phones"]:
        names.append(i['username'])
        numbers.append(j["number"])
for key in names:
    for value in numbers:
        resp[key] = value
        numbers.remove(value)
        break 

for i in data["response"]:
    for j in i["phones"]:
        names.append(i['username'])
        phone_ids.append(j["phone_id"])
for key in names:
    for value in phone_ids:
        phone[key] = value
        phone_ids.remove(value)
        break 



@app.route('/webhook1', methods=['POST','GET'])
def webhook1():
  req = request.get_json(silent=True, force=True)
  fulfillmentText = ''
  query_result = req.get('queryResult')
  user_name = str(query_result.get('parameters').get('sharecreds'))
  number = resp[user_name]
  fulfillmentText = 'Am I chating with '+user_name +'and your registered phone number is '+number

  return {
        "fulfillmentText": fulfillmentText,
        "source": "webhookdata"
    }


@app.route('/webhook2', methods=['POST','GET'])
def webhook2():
  req = request.get_json(silent=True, force=True)
  fulfillmentText = ''
  query_result = req.get('queryResult')
  user_name = str(query_result.get('parameters').get('sharecreds'))
  phone_i = phone[user_name]
  headers = sign("POST","api-22a9c2b4.duosecurity.com","/admin/v1/phones/{phone_i}/send_sms_activation",{},"9m2ySNGIjxlH19m5774ISucb6Bs7xjBW2579neNh","DIS7GXLUI8WCHZU8R0GV")
  requests.post("https://api-22a9c2b4.duosecurity.com//admin/v1/{phone_i}/DPI5J8GYZ9F0KOGHCK8H/send_sms_activation",headers=headers)
  fulfillmentText = 'I have registered you on DUO. You will now recieve an SMS with an activation code for enrolling in DUO'

  return {
        "fulfillmentText": fulfillmentText,
        "source": "webhookdata"
    }
    
   
if __name__ == '__main__':
  app.run(host='0.0.0.0', port=5004)